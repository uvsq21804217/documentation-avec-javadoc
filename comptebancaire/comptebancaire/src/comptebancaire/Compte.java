package comptebancaire;

/** Compte est la class representant un compte bancaire 
 * Un compte est caractériser par un solde
 * Les operation qu'on peux effectuer sur le solde sont :
 * Une consultation
 * Crediter
 * Debiter
 * Un virement
 * @author brahimi lounes
 * @version 1.0
 */
public class Compte {
	/** le solde de Compte est modifiable
	 * @see Compte()
	 * @see Compte(int)
	 * @see crediter(int)
	 * @see debiter(int)
	 * @see virement(Compte, int)
	 */
	private int solde;

	/**
	 * Compte() est le constructeur par default de la class
	 * l'attribut solde est initialiser a 0
	 * 
	 *  @see solde
	 */
	public Compte(){
		this.solde = 0;
	}
	
	/**
	 * constructeur avec une variable int 
	 * @param solde
	 * @see solde
	 * @throws ExceptionSommeNegative si le solde passé en parametre est inferieur à 0
	 */
	public Compte(int solde)throws ExceptionSommeNegative {
		if (solde < 0) throw new ExceptionSommeNegative(); 
		this.solde = solde;
	}
	
	/**
	 * consulter le solde du compte de la class Compte
	 * @return le solde du compte
	 */
	public int consultation() {
		return this.solde;
	}
	/**
	 * credite le solde du compte avec le mentant passé en parametre
	 * @param credit
	 * @see solde
	 * @throws ExceptionSommeNegative si le solde est inferieur à 0
	 */
	public void crediter(int credit) throws ExceptionSommeNegative {
		if (solde < 0) throw new ExceptionSommeNegative();
		this.solde += credit;
	}
	
	/**
	 * debite le solde du compte avec mentant passé en parametre
	 * @param debit
	 * @see solde
	 * @throws ExceptionCompteAdecouvert si le compte est adecouvert
	 */
	public void debiter(int debit) throws ExceptionCompteAdecouvert {
		if ((this.solde - debit) < 0) throw new ExceptionCompteAdecouvert();
		else 
		this.solde -= debit;
	}
	
	/**
	 * Cette fonction permet de faire un virement d'un compte a un autre
	 * @param compte2
	 * @param credit
	 * @throws ExceptionCompteAdecouvert si le compte est adecouvert
	 * @throws ExceptionSommeNegative si le credit passé en parametre est inferieur à 0	
	 */
	public void virement(Compte compte2, int credit)throws ExceptionSommeNegative,ExceptionCompteAdecouvert  {
			if (credit < 0) throw new ExceptionSommeNegative();
			if ((compte2.consultation() - credit )< 0) throw new ExceptionCompteAdecouvert();
			compte2.crediter(credit);
	}
}
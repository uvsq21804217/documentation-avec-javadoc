package comptebancaire;
/** Exception generer si la somme passé en parametre est negative
 * @author brahimi lounes
 * @version 1.0
 */
public class ExceptionSommeNegative extends Exception {
	public ExceptionSommeNegative() {
		System.out.print("Somme negative passé en parametre !");
	}
}

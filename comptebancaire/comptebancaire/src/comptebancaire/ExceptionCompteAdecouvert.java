package comptebancaire;
/** Exception generer si le compte est adecouvert
 * @author brahimi lounes
 * @version 1.0
 */
public class ExceptionCompteAdecouvert extends Exception {
	public ExceptionCompteAdecouvert(){
		System.out.print("Apres cette operation votre compte sera adecouvert ce qui n'est pas autoriser !");
	}
}
